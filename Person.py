import sys
import re


class Person:

    def __init__(self, lines):

        m = re.search("(?P<id>\d{3,7}) (?P<ranking>(SD)|(LD)|I|(LR)|(SR)|(ND)|(U)) (?P<age>\d{2,3}) (?P<sex>[MF])", lines[0])
        n = re.search("(?P<last_name>[A-Za-z]+), (?P<first_name>[A-Za-z]+) ?(?P<middle_initial>[A-Z])?", lines[1])
        o = re.findall("\(\d{3}\) \d{3}-\d{4}", lines[2])
        p = re.search("(?P<address1>\d{1,5} [A-Za-z ]+ )(?P<address2>(Apt|Ste|#|Unit|Lot|Bldg|Trlr) (\w|-)+)? ?(?P<city>Hampton|Poquoson|Yorktown), (?P<state>[A-Z]{2}) (?P<zip>[\d]{5})", lines[3])

        try:
            self.id = m.group('id')

            self.ranking = m.group('ranking')

        except AttributeError as err:
            print("Error: {0} in {1}".format(err, lines))

        self.ranking = m.group('ranking')
        self.age = m.group('age')
        self.sex = m.group('sex')
        self.first_name = n.group('first_name')
        self.last_name = n.group('last_name')
        self.middle_initial = n.group('middle_initial')

        try:
            self.home_phone = o[0]
            self.cell_phone = o[1]
        except IndexError:
            self.home_phone = self.cell_phone = None
        self.address1 = p.group('address1')
        self.address2 = ""
        if p.group('address2'):
            self.address2 = p.group('address2')

        self.city = p.group('city')
        self.state = p.group('state')
        self.zip = p.group('zip')

    def __str__(self):
        return self.first_name + " " + self.last_name

    def csv_headers(self):
        my_vars = vars(self)
        output = ""
        for x in my_vars.keys():
            output = output + x + ", "
        output = output + '\n'
        return output

    def to_csv(self):

        my_vars = vars(self)
        output = ""
        for x in my_vars:
            if my_vars[x] is None:
                output = output + ", "
            else:
                output = output + my_vars[x] + ", "
        output = output + '\n'
        return output


