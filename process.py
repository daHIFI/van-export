from Person import Person
from itertools import zip_longest


def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


persons = []

with open('universe.csv') as f:
    for lines in grouper(f, 5, ''):
        assert len(lines) == 5
        person = Person(lines)
        persons.append(person)


with open('some.csv', 'w', newline='') as f:
    f.write(persons[0].csv_headers())
    for person in persons:
        f.write(person.to_csv())
    f.close()